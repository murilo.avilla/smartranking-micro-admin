import { Injectable, Logger } from '@nestjs/common';
import { Player } from './interfaces/player.schema';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { RpcException } from '@nestjs/microservices';
import { CategoriesService } from 'src/categories/categories.service';

@Injectable()
export class PlayersService {
  private readonly logger = new Logger(PlayersService.name);

  constructor(
    @InjectModel('Player') private readonly playerModel: Model<Player>,
    private readonly categoryService: CategoriesService,
  ) {}

  async createPlayer(player: Player): Promise<Player> {
    try {
      const createdPlayer = new this.playerModel(player);
      return await createdPlayer.save();
    } catch (error) {
      this.logger.error(error);
      throw new RpcException(error.message);
    }
  }

  async findPlayers(_id?: string, categoryId?: string): Promise<Player[]> {
    try {
      const filter: any = {};

      if (_id) filter._id = _id;
      if (categoryId)
        filter.category = await this.categoryService.findCategory(categoryId);

      return await this.playerModel.find(filter);
    } catch (error) {
      this.logger.error(error);
      throw new RpcException(error.message);
    }
  }

  async updatePlayer(_id: string, player: Player): Promise<void> {
    try {
      return await this.playerModel.findOneAndUpdate({ _id }, { $set: player });
    } catch (error) {
      this.logger.error(error);
      throw new RpcException(error.message);
    }
  }

  async deletePlayer(_id: string): Promise<void> {
    try {
      await this.playerModel.deleteOne({ _id });
    } catch (error) {
      this.logger.error(error);
      throw new RpcException(error.message);
    }
  }
}
