import { Controller, Logger } from '@nestjs/common';
import { PlayersService } from './players.service';
import {
  Ctx,
  EventPattern,
  MessagePattern,
  Payload,
  RmqContext,
} from '@nestjs/microservices';
import { Player } from './interfaces/player.schema';

const ackErrors: string[] = ['E11000'];

@Controller()
export class PlayersController {
  constructor(private readonly playersService: PlayersService) {}
  private logger = new Logger(PlayersController.name);

  @EventPattern('player-create')
  async createPlayer(@Payload() player: Player, @Ctx() context: RmqContext) {
    const channel = context.getChannelRef();
    const originalMessage = context.getMessage();

    try {
      this.logger.log(`create ${JSON.stringify(player)}`);
      await this.playersService.createPlayer(player);
      await channel.ack(originalMessage);
    } catch (error) {
      const filterAckError = ackErrors.filter(async (ackError) =>
        error.message.includes(ackError),
      );

      if (filterAckError) await channel.ack(originalMessage);
    }
  }

  @MessagePattern('players-find')
  async findPlayers(@Payload() data: any, @Ctx() context: RmqContext) {
    const channel = context.getChannelRef();
    const originalMessage = context.getMessage();

    try {
      this.logger.log(JSON.stringify(data));
      const { _id, category } = data;
      return await this.playersService.findPlayers(_id, category);
    } finally {
      await channel.ack(originalMessage);
    }
  }

  @EventPattern('player-update')
  async updatePlayer(
    @Payload('id') id: string,
    @Payload('player') player: Player,
    @Ctx() context: RmqContext,
  ) {
    const channel = context.getChannelRef();
    const originalMessage = context.getMessage();

    try {
      this.logger.log(JSON.stringify(player), id);
      await this.playersService.updatePlayer(id, player);
      await channel.ack(originalMessage);
    } catch (error) {
      const filterAckError = ackErrors.filter(async (ackError) =>
        error.message.includes(ackError),
      );

      if (filterAckError) await channel.ack(originalMessage);
    }
  }

  @EventPattern('player-delete')
  async deletePlayer(@Payload() id: string, @Ctx() context: RmqContext) {
    const channel = context.getChannelRef();
    const originalMessage = context.getMessage();

    try {
      this.logger.log(JSON.stringify(id));
      await this.playersService.deletePlayer(id);
      await channel.ack(originalMessage);
    } catch (error) {
      const filterAckError = ackErrors.filter(async (ackError) =>
        error.message.includes(ackError),
      );

      if (filterAckError) await channel.ack(originalMessage);
    }
  }
}
