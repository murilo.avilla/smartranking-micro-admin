import { Controller, Logger } from '@nestjs/common';
import { CategoriesService } from './categories.service';
import {
  Ctx,
  EventPattern,
  MessagePattern,
  Payload,
  RmqContext,
} from '@nestjs/microservices';
import { Category } from 'src/categories/interfaces/category.interface';

const ackErrors: string[] = ['E11000'];

@Controller('categories')
export class CategoriesController {
  constructor(private readonly categoriesService: CategoriesService) {}
  private logger = new Logger(CategoriesController.name);

  @EventPattern('create-category')
  async createCategory(
    @Payload() category: Category,
    @Ctx() context: RmqContext,
  ) {
    const channel = context.getChannelRef();
    const originalMessage = context.getMessage();

    try {
      await this.categoriesService.createCategory(category);
      await channel.ack(originalMessage);
    } catch (error) {
      const filterAckError = ackErrors.filter(async (ackError) =>
        error.message.includes(ackError),
      );
      if (filterAckError) await channel.ack(originalMessage);
    }
  }

  @MessagePattern('categories-find')
  async findCategories(@Payload() _id: string, @Ctx() context: RmqContext) {
    const channel = context.getChannelRef();
    const originalMessage = context.getMessage();

    try {
      if (_id) return await this.categoriesService.findCategory(_id);
      return await this.categoriesService.findCategories();
    } finally {
      await channel.ack(originalMessage);
    }
  }

  @EventPattern('update-category')
  async updateCategory(
    @Payload('category') category: Category,
    @Payload('id') id: string,
    @Ctx() context: RmqContext,
  ) {
    this.logger.log('Started');
    const channel = context.getChannelRef();
    const originalMessage = context.getMessage();

    try {
      await this.categoriesService.updateCategory(id, category);
      await channel.ack(originalMessage);
    } catch (error) {
      const filterAckError = ackErrors.filter(async (ackError) =>
        error.message.includes(ackError),
      );
      if (filterAckError) await channel.ack(originalMessage);
    }
  }
}
